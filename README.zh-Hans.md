# wails-v2-examples

这些示例使用带有Svelte模板的Wails（版本2）。如果您还不熟悉Wails，请参阅https://wails.io/以及https://github.com/wailsapp/wails.

# Prerequisites

请参阅https://wails.io/docs/gettingstarted/installation有关安装墙的说明，请注意支持的平台、依赖关系和特定于平台的依赖关系部分。
然后运行go install github.com/wailsapp/wails/v2/cmd/wails@latest.
建议您运行wails doctor以确定您的系统是否已准备就绪。

# Running the examples

通常，您只需从要运行的示例目录（例如examples/pizza order）运行wails-dev。
但是，这些示例是使用Linux构建的，如果您尝试在Windows上运行它们，则可能会出现错误。在这种情况下，与其简单地运行wails-dev，不如执行以下操作：
而在前端目录`frontend`中执行以下命令：

```bash
npm install

npm run build
```

然后在项目目录中执行以下命令：

```bash
go build -tags dev -gcflags "all=-N -l"

wails dev
```

在未来的Wails版本中可能不需要这些额外的步骤。使用MacOS可能不需要它们。用于构建这些示例的Wails版本是v2.0.0-beta.37。
请注意，为了使示例按预期运行，nation-db示例有特殊要求。有关更多信息，请参阅examples/nation-db中的README.md文件。模拟国家数据库示例没有这些要求。

## Another Example

对于更高级的示例，您可能想查看RiftShare(https://riftshare.app/ 以及 https://github.com/achhabra2/riftshare/tree/v0.1.9).